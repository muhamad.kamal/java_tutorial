# Java Language



## Apa Itu Java
Java Adalah bahasa pemrograman yang populer. Dibuat atau didirikan pada tahun 1995 yang dimiliki oleh oracle.

## Java Digunakan untuk : 
- Mobile Application
- Dekstop Application
- Website
- Web server
- Games
- Data Base Connection

## Mengapa Java
- Java bersifat general purpose yang artinya java tidak hanya dipakai untuk membuat program spesifik saja
- Java Itu Cross Platform atau dapat berjalan di atas platform yang berbeda beda (linux , Windows , MacOs)
- Java merupakan bahasa pemrograman yang berorientasi object

## Sejarah Java 
Bahasa ini awalnya dibuat oleh james Gosling saat masih bergabung dengan Sun Microsystem pada tahun 1991. Saat ini merupakan bagian dari oracle dan dirilis pada tahun 1995. Bahasa ini mengadopsi sintaksis bahasa pemrograman C dan C++.

## Teknologi Java 
- Java Standar Edition (SE)
- Java Enterpris Edition (EE)
- Java Micro Edition (ME)


## Cara Kerja Java
- Menulis code program (source code)
- Melakukan compile (mengubah source code menjadi byte code / bahasa mesin)
- menjalankan program pada JVM (Java virtual machine) merupakan mesin virtual untuk menjalankan Java.
