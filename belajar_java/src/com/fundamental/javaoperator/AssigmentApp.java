package com.fundamental.javaoperator;

public class AssigmentApp {
    /*
        Java Assigment
        Operator penugasan (Assignment Operator) fungsinya untuk meberikan tugas pada variabel tertentu.
        Biasanya untuk mengisi nilai.

        Nama Operator	               Sombol
        =====================================
        Pengisian Nilai	                =
        Pengisian dan Penambahan	    +=
        Pengisian dan Pengurangan	    -=
        Pengisian dan Perkalian	        *=
        Pengisian dan Pembagian	        /=
        Pengisian dan Sisa bagi	        %=
        =====================================

    */

    public static void main(String[] args) {
        int a , b;

        // Pengisian nilai
        a = 10;
        b = 5;

        // Penambahan
        a += b;
        System.out.println(a);

        // Pengurangan
        a -=b;
        System.out.println(a);

        // Perkalian
        a *= b;
        System.out.println(a);

        // pembagian
        a /= b;
        System.out.println(a);

        //modulus
        a %=b ;
        System.out.println(a);

        //==================================================
        a = 5;
        a += 15;
        System.out.println(a);

    }
}
