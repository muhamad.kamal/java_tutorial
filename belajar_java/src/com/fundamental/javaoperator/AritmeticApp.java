package com.fundamental.javaoperator;

public class AritmeticApp {
     /*
        Operator Aritmetic
        +	Addition       || Adds together two values	                ||  x + y
        -	Subtraction	   || Subtracts one value from another          ||	x - y
        *	Multiplication || Multiplies two values                     ||	x * y
        /	Division	   || Divides one value by another	            ||  x / y
        %	Modulus	       || Returns the division remainder	        ||  x % y
        ++	Increment	   || Increases the value of a variable by 1	|| ++x
        --	Decrement	   || Decreases the value of a variable by 1	|| --x
     */

    public static void main(String[] args) {
        // Example
        int a,b,c,results;
        a = 10;
        b = 5;
        c = 9;

        results = a + b * c; // 10 + 5 * 9
        System.out.println(results); // 55
    }
}
