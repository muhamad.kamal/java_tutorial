package com.fundamental.javaoutput;

public class Main {
    /*
        Java Output
        fungsi untuk menampilkan nilai (nilai variable  / teks)
        Ada beberapa fungsi / method yang sudah disediakan oleh Java:
        - Fungsi System.out.print()
        - Fungsi System.out.println()
        - Fungsi System.out.format() / System.out.printf()
    */
    public static void main(String[] args) {

        // print
        System.out.print("Hallo Everyone \n");

        // println
        System.out.println("Hallo Everyone");

        // printf
        System.out.printf("Hallo %s","Everyone");
    }
}
