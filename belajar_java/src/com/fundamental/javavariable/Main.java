package com.fundamental.javavariable;

public class Main {

    /*
        Java Variable
        Variabel adalah wadah untuk menyimpan nilai data.

        Di Java, ada berbagai jenis variabel, misalnya:
        - String- menyimpan teks, seperti "Halo". Nilai string dikelilingi oleh tanda kutip ganda
        - int- menyimpan bilangan bulat (bilangan bulat), tanpa desimal, seperti 123 atau -123
        - float- menyimpan angka floating point, dengan desimal, seperti 19,99 atau -19,99
        - char- menyimpan karakter tunggal, seperti 'a' atau 'B'. Nilai karakter dikelilingi oleh tanda kutip tunggal
        - boolean- menyimpan nilai dengan dua status: benar atau salah


         Syntax
         ==============================
         typeData Varibale = Value
         ==============================
    */

    public static void main(String[] args) {
        // teks
        String name  = "Muhamad Badru Kamal";

        // number
        int age = 19;

        System.out.println("Hallo my name is " + name);
        System.out.println("My age is " + age);
    }
}
