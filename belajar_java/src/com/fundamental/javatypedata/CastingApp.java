package com.fundamental.javatypedata;

public class CastingApp {
    /*

        In Java, there are two types of casting:

        Widening Casting (automatically) - converting a smaller type to a larger type size
        byte -> short -> char -> int -> long -> float -> double

        Narrowing Casting (manually) - converting a larger type to a smaller size type
        double -> float -> long -> int -> char -> short -> byte

    */

    public static void main(String[] args) {
        // Widening Casting
        int numberInt = 9;
        double numberDouble = numberInt;
        System.out.println(numberInt);
        System.out.println(numberDouble);

        // Narrowing Casting
        double numberDouble2 = 8.7;
        int numberInt2 = (int)numberDouble2;
        System.out.println(numberDouble2);
        System.out.println(numberInt2);
    }
}
