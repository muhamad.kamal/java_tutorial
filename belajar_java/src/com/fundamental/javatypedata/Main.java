package com.fundamental.javatypedata;

public class Main {
    /*
        Java Tipe Data

        int myNum = 5;               // Integer (whole number)
        float myFloatNum = 5.99f;    // Floating point number
        char myLetter = 'D';         // Character
        boolean myBool = true;       // Boolean
        String myText = "Hello";     // String

        Data types are divided into two groups:
        - Primitive data types - includes byte, short, int, long, float, double, boolean and char
        - Non-primitive data types - such as String, Arrays and Classes

    */

    public static void main(String[] args) {
        int myInt =   10;
        System.out.println(myInt); // menampilkan nilai int
        System.out.println(Integer.MAX_VALUE); // ukuran max int
        System.out.println(Integer.MIN_VALUE); // ukuran min int
        System.out.println(Integer.SIZE); // ukuran dari int
        System.out.println(Integer.BYTES); // ukuran dalam bentuk byte
    }
}
